import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';

import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

WebFont.load({
    google: {
      families: ['Quicksand:300', 'Monoton', 'Yesteryear']
    }
  });
