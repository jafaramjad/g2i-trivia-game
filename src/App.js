import React, { Component } from 'react';
import styled from 'styled-components';

import Trivia from './components/Trivia/Trivia';

let AppContainer = styled.div`
  padding: 0px;
`;

class App extends Component {
  render() {
    return (
      <AppContainer>
        <Trivia />
      </AppContainer>
    );
  }
}

export default App;
