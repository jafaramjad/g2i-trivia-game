import * as React from 'react';
import styled from 'styled-components';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

let ResultIconSpan = styled.span`
    display: inline-block;
    text-align: left;
    font-size: 32px;
    padding-right: 10px;
`;

let ResultQuestionText = styled.span`
    font-size: 18px;
    font-weight: bold;
    color: #333;
`;

let ResultAnswers = styled.p`
    color: #999;
    padding-left: 30px;
`;

let ResultListItem =styled.li`list-style-type: none;`;


let ResultList = styled.ul`
    padding: 0px;

    .Correct ${ResultIconSpan} { color: green;}
    .Wrong ${ResultIconSpan} {color: red};

    .Correct ${ResultQuestionText} { text-decoration: none;}
    .Wrong ${ResultQuestionText} { text-decoration: line-through};
`;

const TriviaResultList = (props) => {

    let resultList = props.resultList;
    let correctAnswers = props.correctAnswers;
    let userAnswers = props.userAnswers;

    return(
        <ResultList>
        {props.realAnswers.map((val,idx)=>{
            return(
                
                <ResultListItem key={`#Q${idx}`} className={resultList[idx]}>
                  <ExpansionPanel>

                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <ResultIconSpan>{resultList[idx] === 'Correct' ? '+' : 'x'}</ResultIconSpan>
                        <Typography dangerouslySetInnerHTML={{__html: val.question }} />
                    </ExpansionPanelSummary>

                    <ExpansionPanelDetails>
                        <ResultAnswers>
                            You: <span dangerouslySetInnerHTML={{__html: userAnswers[idx] }}></span>
                            <br />
                            Answer: <span dangerouslySetInnerHTML={{__html: correctAnswers[idx]  }}></span>
                        </ResultAnswers>
                    </ExpansionPanelDetails>

                  </ExpansionPanel>
                </ResultListItem>
            );
        })}
        </ResultList>
    )
}

export default TriviaResultList;