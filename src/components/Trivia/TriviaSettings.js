import * as React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Settings from '../../constants/Settings';

let SettingsContainer = styled.div`
    display: ${props => props.showSettings ? 'block' : 'none'  }
`;


let SaveBtn = styled(Button)`
    margin-top: 20px !important;
`;


class TriviaSettings extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            settingsType: `${Settings.type}`,
            settingsDifficulty: `${Settings.difficulty}`,            
            settingsAmount: `${Settings.amount}`
        }
    }

    /* CLICK SAVE BTN */
    saveSettings = () => {

        let currentSettings = {
            "amount": this.state.settingsAmount,
            "type": this.state.settingsType,
            "difficulty": this.state.settingsDifficulty
        }

        this.props.handleClickSettings(currentSettings);
    }

    /* AMOUNT CHANGE */
    handleAmountChange = (evt) => {
        this.setState({
            settingsAmount: evt.target.value,
        })
    }

    /* TYPE CHANGE */
    handleTypeChange = (evt) => {
        this.setState({
            settingsType: evt.target.value,
        })
    }


    /* DIFFICULTY CHANGE */
    handleDifficultyChange = (evt) => {
        this.setState({
            settingsDifficulty: evt.target.value,
        })
    }




     render(){

        return(
            <SettingsContainer showSettings={this.props.display}>
                <h1>Settings</h1>


                <FormLabel component="legend">Amount</FormLabel>
                <RadioGroup
                    row={true}
                    aria-label="settings-amount"
                    name="settings-type"
                    value={this.state.settingsAmount}
                    onChange={this.handleAmountChange}>
                    
                    <FormControlLabel value="5" control={<Radio />} label="5" />
                    <FormControlLabel value="10" control={<Radio />} label="10" />
                    <FormControlLabel value="25" control={<Radio />} label="25" />
                    <FormControlLabel value="50" control={<Radio />} label="50" />

                 </RadioGroup>

                <FormLabel component="legend">Type</FormLabel>
                <RadioGroup
                    row={true}
                    aria-label="settings-type"
                    name="settings-type"
                    value={this.state.settingsType}
                    onChange={this.handleTypeChange}>
                    
                    <FormControlLabel value="boolean" control={<Radio />} label="True/False" />
                    <FormControlLabel value="multiple" control={<Radio />} label="Multiple Choice" />
                  
                 </RadioGroup>


                <FormLabel component="legend">Difficulty</FormLabel>
                <RadioGroup
                    row={true}
                    aria-label="settings-difficulty"
                    name="settings-difficulty"
                    value={this.state.settingsDifficulty}
                    onChange={this.handleDifficultyChange}>
                    
                    <FormControlLabel value="easy" control={<Radio />} label="Easy" />
                    <FormControlLabel value="hard" control={<Radio />} label="Hard" />
                  
                 </RadioGroup>


            <SaveBtn 
                onClick={this.saveSettings} 
                variant="outlined" 
                fullWidth={true} 
                color="primary" 
                size="large">Save</SaveBtn>
            </SettingsContainer>
        );
    }
}

export default TriviaSettings;