import * as React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import CircularProgressbar from 'react-circular-progressbar';
import TriviaResultList from './TriviaResultList';

let ResultContainer = styled.div` display: ${props => props.showResult ? 'block' : 'none'  }`;

let Title = styled.h1`text-align: center;`;

let NumCorrect = styled.h5`text-align: center;`;

let CircularContainer = styled.div`
    width: 30%;
    margin: 0px auto;

    .CircularProgressbar-path   { stroke: #4253AF; }
    .CircularProgressbar-text   { fill: #4253AF; }
`;

let RetryBtn = styled(Button)``;


const TriviaResult = (props) => {
    let userAnswers = props.userAnswers;
    let realAnswers = props.questions.results;
    let correctAnswers = [];
    let resultList = [];
    let numAnsweredCorrectly = '';
    let percentCorrect;


    /* GET REAL ANSWERS AND COMPARE */
    if(props.display){

        //MAP
         realAnswers.map((val,idx)=>{

            //CHECK ANSWERS
            resultList = [...resultList,  val.correct_answer === userAnswers[idx] ? 'Correct' : 'Wrong'];
                    
            //STORE
            correctAnswers = [...correctAnswers,  val.correct_answer];
            
            return correctAnswers;

        //END MAP
        });

        /* NUMBER OF CORRECT ANSWERS */
        numAnsweredCorrectly  =  resultList.filter((val) => { return val === 'Correct' }).length;

        percentCorrect =  Math.floor(numAnsweredCorrectly/resultList.length * 100);
    }


    let clickRetry = () => {
        props.handleRetry();
    }

    return(
        <ResultContainer showResult={props.display}>
            
            
            <Title>Results</Title>

            <CircularContainer>
                <CircularProgressbar percentage={percentCorrect} />
            </CircularContainer>

           <NumCorrect>You got {numAnsweredCorrectly && numAnsweredCorrectly} out of {realAnswers && realAnswers.length} questions correct</NumCorrect>

           <RetryBtn onClick={clickRetry} variant="raised" fullWidth={true} color="default" size="large">Play Again?</RetryBtn>

            {props.display && 
            <TriviaResultList 
                realAnswers={realAnswers} 
                resultList={resultList} 
                correctAnswers={correctAnswers} 
                userAnswers={userAnswers} /> 
            }


        </ResultContainer>
    )
}


export default TriviaResult;