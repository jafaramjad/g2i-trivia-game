import * as React from 'react';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Theme from '../../constants/Theme';


let CardContainer = styled(Card)`
    margin: 20px;
    background: #f00;
    font-family: ${Theme.font2} !important;

`;

let TriviaCard = (props) => {


    return(
        <Grid container spacing={0}  justify={'center'}>
            <Grid item xl={8} lg={6} md={9} sm={10} xs={12}>
                <CardContainer >
                    <CardContent>
                        {props.children}
                    </CardContent>
                </CardContainer>
            </Grid>
        </Grid>
    )
}

export default TriviaCard;