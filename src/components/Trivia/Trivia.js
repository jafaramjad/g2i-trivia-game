import * as React from 'react';
import axios from 'axios';

import TriviaCard from './TriviaCard';
import TriviaStart from './TriviaStart';
import TriviaQuestion from './TriviaQuestion';
import TriviaResult from './TriviaResult';
import TriviaSettings from './TriviaSettings';
import Settings from '../../constants/Settings';

class Trivia extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            questionType: `${Settings.type}`,
            questionDifficulty: `${Settings.difficulty}`,
            questionAmount: `${Settings.amount}`,
            questionData: null,
            showSettings: false,
            showStart: true,
            showQuestion: false,
            showResults: false,
            userAnswers: null,

        }
    }

    componentDidMount(){

        this.getQuestionData();

    }
    
    /* GET QUESTION DATA */
    getQuestionData = () => {

        axios({
            method: 'GET',
            url: `https://opentdb.com/api.php?amount=${this.state.questionAmount}&difficulty=${this.state.questionDifficulty}&type=${this.state.questionType}`,
        
        }).then((response)=> {
            this.setState({ questionData: response.data });

        }).catch((error)=> {
            console.log(error);
        });
    }



    /* STARTING TRIVIA */
    handleClickStart = () => {
        this.setState({
            showStart: false,
            showQuestion: true
        })
    }

    /* TRIVIA HAS FINISHED */
    handleResults = (userAnswers) => {
        this.setState({
            showQuestion: false,
            showResults: true,
            userAnswers: userAnswers
        })
    }

    /* RESTART TRIVIA */
    handleClickRetry = () => {
        this.getQuestionData();

        this.setState({
            showStart: true,
            showQuestion: false,
            showResults: false

        })
    }

    /* CLICK SETTINGS BTN */
    handleClickSettings = () => {
        console.log('Clicked Settings button')

        this.setState({
            showSettings: true,
            showStart: false,
            showQuestion: false,
            showResults: false
        })
    }

/* HANDLE SAVE SETTINGS */
handleSaveSettings = (currentSettings) => {

    console.log('Current Settings Saved..');
    console.log(currentSettings);

    this.setState({
        showSettings: false,
        showStart: true,
        showQuestion: false,
        showResults: false,
        questionType: currentSettings.type,
        questionDifficulty: currentSettings.difficulty,
        questionAmount: currentSettings.amount
    }, ()=>{
        this.getQuestionData();
    })
}


   render(){
        return(
              
                <TriviaCard>
                 
                     <TriviaSettings
                        display={this.state.showSettings}
                        handleClickSettings={this.handleSaveSettings}
                    /> 

                    <TriviaStart  
                        display={this.state.showStart} 
                        handleClickStart={this.handleClickStart}
                        handleClickSettings={this.handleClickSettings}
                        questionType={this.state.questionType === 'boolean' ? 'True or False' : 'Multiple Choice'}
                        questionAmount={this.state.questionAmount} />

                    {!this.state.showResults && this.state.showQuestion && 
                    <TriviaQuestion 
                        display={this.state.showQuestion} 
                        handleResults={this.handleResults} 
                        questions={this.state.questionData}  /> }

                    {this.state.showResults &&
                    <TriviaResult  
                        display={this.state.showResults} 
                        handleRetry={this.handleClickRetry} 
                        questions={this.state.questionData} 
                        userAnswers={this.state.userAnswers} />}

                </TriviaCard>
        )
    }
}


export default Trivia;