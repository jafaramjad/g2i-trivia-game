import * as React from 'react';
import styled from 'styled-components';
import TriviaQuestionAnswers from './TriviaQuestionAnswers';

let QuestionContainer = styled.div`
    display: ${props => props.showQuestion ? 'block' : 'none'  }
`;

let Category = styled.h1`
    font-size: 20px;
    font-weight: 600;
`;

let Question = styled.p`
    font-size: 30px;
    @media (max-width: 600px) {
        font-size: 22px;
    }
`;


class TriviaQuestion extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            userAnswers: [],
            gen: null,
            genItem: null,
            genIdx: 0,
            questionList: null,
            questionLength: null
        }
    }

    componentDidMount(){
        this.setState({
            questionList: this.props.questions.results,
            questionLength: this.props.questions.results.length,
        })
        this.initialQuestion();
    }

    /* GENERATOR */
    *questionGen(array){
        let index = 0;
        while(index <= array.length){
            yield { "item": array[index], "idx": index++ }
        }
    }

    /* START FIRST QUESTION */
    initialQuestion(){
        let gen = this.questionGen(this.props.questions.results);
        let genNext = gen.next();

        this.setState({
            gen: gen,
            genItem: genNext.value.item,
            genIdx: genNext.value.idx + 1
        })
    }

    /* CLICK/NEXT QUESTION */
    clickAnswer = (answer) => {

        let currentUserAnswersList = [...this.state.userAnswers, answer];
        let genNext = this.state.gen.next();

        this.setState({
            userAnswers: currentUserAnswersList,
            genItem: genNext.value.item,
            genIdx:  genNext.value.idx + 1
        });
        
        /* CHECK RESULTS */
        if(this.state.genIdx === this.state.questionLength){
            this.props.handleResults(currentUserAnswersList);
        }

    }

 
render(){
    return(
        <QuestionContainer showQuestion={this.props.display}>
          
                <Category>{this.state.genItem && this.state.genItem.category}</Category>

                {this.state.genItem && 
                <Question dangerouslySetInnerHTML={{__html: this.state.genItem.question}}></Question> }

           
               
                <TriviaQuestionAnswers 
                    handleClick={this.clickAnswer} 
                    questionType={this.state.genItem && this.state.genItem.type}
                    questionAnswers={this.state.genItem && [this.state.genItem.correct_answer, ...this.state.genItem.incorrect_answers]} />

               

                <h4>{this.state.genIdx} of {this.state.questionLength}</h4>
           
        </QuestionContainer>
    )
    }
}


export default TriviaQuestion;
