import * as React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Theme from '../../constants/Theme';

let StartContainer = styled.div`
    text-align: center;
    display: ${props => props.showStart ? 'block' : 'none'  }
`;


let LogoImg = styled.img`
    height: 200px;
`;

let Title1 = styled.h1`
    font-family: ${Theme.font1};
    font-size: 124px;
    font-weight: 300;
    color: #4253AF;
    line-height: 100%;
    margin: 0px;
    text-transform: lowercase;
    text-align: center;
    @media (max-width: 600px) {
        font-size: 72px;
    }
`;

let Title2 = styled.h1`
    font-family: ${Theme.font1};
    font-size: 70px;
    font-weight: 300;
    color: #DC5593;
    line-height: 100%;
    margin: 0px;
    padding: 0px;
    text-transform: lowercase;
    text-align: center;
    @media (max-width: 600px) {
        font-size: 40px;
    }
`;

let Slogan = styled.h3`
    font-family: ${Theme.font2};
    font-weight: 300;
    font-size: 24px;
    margin: 0px;
    margin-bottom: 30px;
`;

let Instructions = styled.div`
    text-align: center;
`;

let StartBtn = styled(Button)`
    margin-top: 20px !important;
`;

let SettingsBtn = styled(Button)`
    margin-top: 20px !important;
`;


const TriviaStart = (props) => {

    let clickStart = () => {
        props.handleClickStart();
    }

    let clickSettings = () => {
        props.handleClickSettings();
    }

    return(
        <StartContainer showStart={props.display}>

            <LogoImg src="/images/TriviaBrain.png" />
            <Title1>Trivia</Title1>
            <Title2>Challenge</Title2>
            <Slogan>Can you score 100%</Slogan>

            <Instructions>You will be presented with {props.questionAmount} {props.questionType} questions.</Instructions>

            <StartBtn 
                onClick={clickStart} 
                variant="contained" 
                fullWidth={true} 
                color="primary" 
                size="large">Start</StartBtn>
            <SettingsBtn 
                onClick={clickSettings} 
                variant="outlined" 
                fullWidth={true} 
                color="secondary" 
                size="large">Settings</SettingsBtn>
        </StartContainer>
    )
}


export default TriviaStart;