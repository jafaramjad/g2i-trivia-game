import * as React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';

let QuestionBtn = styled(Button)`
    margin: 10px 0px !important;
`;

const TriviaQuestionAnswers = (props) => {

    
    /* SHUFFLE ARRAY - FISHER/YATES */
    let shuffleArray = (arr) => {
        let ctr = arr.length;
        while (ctr > 0) {
            let idx = Math.floor(Math.random() * ctr);
            ctr--;

             // SWAP
            let tmp = arr[ctr];
            arr[ctr] = arr[idx];
            arr[idx] = tmp;
        }
        return arr;
    }


    
    /* MULTIPLE */
    if(props.questionType === 'multiple'){

        let shuffledQuestions = shuffleArray(props.questionAnswers);
        console.log(props.questionAnswers);
        console.log(shuffledQuestions);

        return(
            <div>
            {props.questionAnswers.map((val,idx) => {
               return( <QuestionBtn 
                    key={`MultiAnswer${idx}`}
                    onClick={()=>{props.handleClick(val)}}
                    fullWidth={true}
                    variant="outlined" 
                    color="secondary" 
                    size="small" >
                        <div dangerouslySetInnerHTML={{__html: val}} />
                    </QuestionBtn>
               )
            })}
            </div>
        )

    /* BOOLEAN */    
    } else {

        return(
            <div>
                <QuestionBtn 
                    onClick={()=>{props.handleClick('True')}} 
                    fullWidth={true}
                    variant="outlined" 
                    color="primary" 
                    size="small">True</QuestionBtn>

                  <QuestionBtn 
                    onClick={()=>{props.handleClick('False')}} 
                    fullWidth={true}
                    variant="outlined" 
                    color="secondary" 
                    size="small">False</QuestionBtn>
            </div>
        )
    }
}

export default TriviaQuestionAnswers;