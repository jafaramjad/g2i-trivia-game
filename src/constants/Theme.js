const Theme = {
    "color1": '#333',
    "color2": "#999",
    "color3": "000",
    "font1": "Monoton",
    "font2": "Quicksand",
    "font3": "Yesteryear"
};

export default Theme;